var path = require('path');
var webpack = require('webpack');
var extractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        home: './app.ts'
    },
    output: {
        filename: '[name].js',
        library: "[name]",
        path: path.resolve(__dirname, 'dist')
    },
    watch: true,
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel"
            }, {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }, {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: extractTextPlugin.extract({
                    fallback: 'style',
                    use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            }
        ]
    },
    externals: {
        lodash: {
            root: '_'
        }
    },
    resolve: {
        modules: ['node_modules'],
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    resolveLoader: {
        modules: ['node_modules'],
        extensions: [' ', '.js'],
        moduleExtensions: ['-loader']
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common'
        }),
        new extractTextPlugin('style.css')
    ]
};